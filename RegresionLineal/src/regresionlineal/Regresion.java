/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regresionlineal;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 * @author Sebastian Olarte - 1151942
 * @author Hernando Zuluaga - 1151842
 */
public class Regresion {
    
    double independiente[];
    double dependiente[];
    int numeroDatos;

    public Regresion() {
        this.definirDatos();
        this.independiente = new double[this.numeroDatos];
        this.dependiente = new double[this.numeroDatos];
        this.llenarDatos();
        this.ecuacionFinal();
        
        
    }

    private void definirDatos(){
    this.numeroDatos = Integer.parseInt(JOptionPane.showInputDialog("Ingresa numero de datos a usar"));
    }
    
    private void llenarDatos(){
        for (int i = 0; i < this.independiente.length; i++) {
            independiente[i] = Double.parseDouble(JOptionPane.showInputDialog(""+(i+1)+" Dato independiente "));
        }
        
        for (int i = 0; i < this.dependiente.length; i++) {
            dependiente[i] = Double.parseDouble(JOptionPane.showInputDialog(""+(i+1)+" Dato dependiente "));
        }
    }
    
    private double hallarMediaIndependiente(){
        double media = 0;
        for (int i = 0; i < this.independiente.length; i++) {
            media+=independiente[i];
        }
        
        media = media/this.numeroDatos;
        
        return media;
    }
    
    private double hallarMediaDependiente(){
        double media = 0;
        for (int i = 0; i < this.dependiente.length; i++) {
            media+=dependiente[i];
        }
        
        media = media/this.numeroDatos;
        
        return media;
    }
    
    private double hallarSumatoriaProductos(){
        double mediaInd = this.hallarMediaIndependiente();
        double mediaDep = this.hallarMediaDependiente();
        double sumatoria = 0;
        
        for (int i = 0; i < this.numeroDatos; i++) {
            sumatoria+=((this.independiente[i]-mediaInd) * (this.dependiente[i]-mediaDep));
        }
        
        return sumatoria;
    }
    
    private double hallarDesviacionX (){
        double media = this.hallarMediaIndependiente();
        double desviacion = 0;
        
        for (int i = 0; i < this.numeroDatos; i++) {
            double aux = Math.pow(this.independiente[i] - media, 2) ; 
            desviacion+=aux;
        }
        
        return desviacion; 
    }
    
    private double b1 (){
        return this.hallarSumatoriaProductos() / this.hallarDesviacionX();
    }
    
    private double b0 (){
        return this.hallarMediaDependiente() - (this.b1() * this.hallarMediaIndependiente());
    }
    
    private void ecuacionFinal (){
        DecimalFormat formato1 = new DecimalFormat("#0.00");
        String msg = "y = "+ formato1.format(this.b0()) +" + "+ formato1.format(this.b1())+"x";
        if (this.b1() <0) {
            msg = "y = "+ formato1.format(this.b0()) +" "+ formato1.format(this.b1())+"x";
        }
        JOptionPane.showMessageDialog(null, msg);
    }
    
    
}
